import axios from "axios";

export const getActivity = async() => {

    try {
        const response = await axios.get("http://www.boredapi.com/api/activity/");

        if(response.status !== 200)
        {
            throw new Error("COuld not complete request");
        }
        return [null, response.data]
    } 
    
    catch (error) {
        return [error.message, []]
    }
}