import { Card } from "react-bootstrap";

const Activity = ({ activity }) => {

    return (
        <>
            <Card>
                <Card.Body>
                    <Card.Title>
                        Activity: {activity.activity}
                    </Card.Title>
                    <Card.Text>
                        <p>Type: {activity.type}</p>
                        <p>Participants: {activity.participants}</p>
                        <p>Price: {activity.price}</p>
                    </Card.Text>
                </Card.Body>
            </Card>

        </>
    )
}


export default Activity;